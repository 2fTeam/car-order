package com.example.first_in_yukon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FirstInYukonApplication {

	public static void main(String[] args) {
		SpringApplication.run(FirstInYukonApplication.class, args);
	}

}
