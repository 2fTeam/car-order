package com.example.first_in_yukon.repo;

import com.example.first_in_yukon.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}
