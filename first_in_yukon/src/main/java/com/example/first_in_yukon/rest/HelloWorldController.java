package com.example.first_in_yukon.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {
    @RequestMapping("/")
    public String getHelloWorldStr() {
        return "Hello world";
    }
}
